let app = angular.module('crud-app', []);

app.controller('crud-ctrl', function ($scope) {
    $scope.people = [];
    let empid = 1;

    $scope.saveRecord = function () {

        if (!validateFullname()) {
            alert("Invalid Fullname!")
        } else if (!validateUsername()) {
            alert("Invalid Username!");
        } else if (!validatePassword()) {
            alert("Invalid Password!");
        } else if (!checkPasswordMatch()) {
            alert("Passwords dont match!");
        } else if (!validateAddress()) {
            alert("Invalid Address!");
        } else if (!validateAge()) {
            alert("Invalid Age!");
        } else {
            if ($scope.person.id == null) {

                $scope.person.id = empid++;

                $scope.people.push($scope.person);

            } else {

                for (i in $scope.people) {

                    if ($scope.people[i].id == $scope.person.id) {

                        $scope.people[i] = $scope.person;

                    }

                }

            }

            $scope.person = {};
            document.getElementById('id01').style.display = 'none';
        }

    }

    $scope.delete = function (id) {

        for (i in $scope.people) {

            if ($scope.people[i].id == id) {

                $scope.people.splice(i, 1);

                $scope.person = {};

            }

        }

    }

    $scope.edit = function (id) {

        for (let i in $scope.people) {

            if ($scope.people[i].id == id) {

                $scope.person = angular.copy($scope.people[i]);

            }

            document.getElementById('id01').style.display = 'block';
        }

    }

    $scope.clearr = function () {
        $scope.person.id = null;
        document.getElementById('id01').style.display='none';
    }

    $scope.enableForm = function() {
        document.getElementById("dataForm").reset();
        document.getElementById('id01').style.display = 'block';
    }
});

function validatePassword() {
    let password1 = document.getElementById("password").value;
    let password2 = document.getElementById("repassword").value;

    if (password1.length <= 5 || password1.length >= 51 || password2.length <= 5 || password2.length >= 51) {
        return false;
    } else {
        return true;
    }
}

function checkPasswordMatch() {
    let password1 = document.getElementById("password").value;
    let password2 = document.getElementById("repassword").value;

    if (password1 != password2) {
        return false;
    } else {
        return true;
    }
}

function validateUsername() {

    let username = document.getElementById("username").value;
    let format = /^[a-zA-Z0-9_]*$/;

    if (username === "" || username.length > 200 || !format.test(username)) {
        return false;
    } else {
        return true;
    }
}

function validateAge() {
    let age = document.getElementById("age").value;
    let format = /^[0-9]*$/;

    if (!format.test(age)) {
        return false;
    } else {
        if (Number(age) == 0) {
            return false;
        }
        return true;
    }
}

function validateFullname() {
    let fullname = document.getElementById("fullname").value;

    if (fullname === "" || fullname.length > 200) {
        return false;
    } else {
        return true;
    }
}

function validateAddress() {
    let address = document.getElementById("address").value;

    if (address === "" || address.length > 500) {
        return false;
    } else {
        return true;
    }
}
