function matchPassword() {
    let password1 = document.querySelector("input[name=password]");
    let password2 = document.querySelector("input[name=repassword]");

    if (password1.value != password2.value) {
        password2.setCustomValidity("Password did not match")
    } else {
        password2.setCustomValidity("");
    }
}

function validateDob() {
    let dob = document.getElementById("dob").value;
    let varDate = new Date(dob);
    let today = new Date();
    today.setHours(0, 0, 0, 0);

    if (varDate >= today) {
        alert("Date of Birth is invalid!");
        document.getElementById("dob").value = "";
    }
}